require 'json'
$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "network/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "network"
  s.version     = Network::VERSION
  s.authors     = ["Tobias Grasse"]
  s.email       = ["tg@glancr.de"]
  s.homepage    = "https://glancr.de/modules/network"
  s.summary     = "mirr.OS widget to display network connection info."
  s.description = "Shows your current network connection: WiFi name, signal strength and IP address."
  s.license     = "MIT"
  s.metadata    = { 'json' =>
                  {
                    type: 'widgets',
                    title: {
                      enGb: 'Network',
                      deDe: 'Netzwerk'

                    },
                    description: {
                      enGb: s.description,
                      deDe: 'Zeigt deine aktuelle Netzwerkverbindung: WLAN-Name, Signalstärke und IP-Adresse.'
                      # Add description for other languages keyed by the snakeCased language tag.
                    },
                    sizes: [
                      {
                        w: 4,
                        h: 2
                       }
                    ],
                    languages: [:enGb, :deDe], # Add snake-cased language tags if your templates have the locales for it.
                    group: nil # see https://gitlab.com/glancr/mirros_one/wikis/home for available groups
                  }.to_json
                }

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_development_dependency 'rails', '~> 5.2'
end
