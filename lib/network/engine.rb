module Network
  class Engine < ::Rails::Engine
    isolate_namespace Network
    config.generators.api_only = true
  end
end
